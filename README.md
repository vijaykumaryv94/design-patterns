# Design Patterns in Python

This is a Technical Paper on 5 Design Patterns  used commonly in Python. 


* What is a **Design Pattern?**
    * Design Patterns in software developement became popular after the famously known as'Gang of Four': Gamma, Helm, Johnson, Vlissides Published their book Design Patterns: Elements of reusable Object Orient Software in 90s. Design Patterns are the essential part of Software Development Life Cycle. They provide a general description or reusable solution that can used to a commonly recurring problem in software design. Design Patterns are not by themselves a complete solution that developers can use and code directly. They are kind of template that describes how to solve a particular problem scenario in real world. They show the interactions between classes and objects. The advantage of sound knowledge of Design Patterns is that, we can speed up the development process by using some proven design paradigms. By using various Design Patterns our code becomes more flexible, reusable and maintainable. There are mainly 3 types of design patterns available. They are 1) Creational 2) Structural 3) Behavioral.
    * **Creational:** These Patterns are all about class instantiation or Object creation. They can be either Class-creational or Object-creational patterns.
      *  Ex:- Factory Method, Abstract Method, Singleton, Prototype, Builder and Object Pool etc.
   *  **Structural:** The main goal of these patterns is to organise different classes and objects to better structures and increase the functionality of classes involved. These are designed with a regard to Class's structure and composition.
      *  Ex:- Facade, Adapter, Bridge, Composite, Decorator, Flyweight and Proxy etc.
   *  **Behavioral:** These Patterns are designed based on how classes and objects communicates with other. The patterns are about identifying the common communication patterns between objects and realize them.
      *  Ex:- Template Method, Observer, Memento, Mediator, Interpreter, Visitor and Strategy etc.

* Let us discuss 5 commonly used Design Patterns.
 1. **Singleton Pattern** 
    1. This pattern comes under Creational Design Pattern. In many real world scenarios we want to create only one instance of a class. For example a class of President of a country has only one instance of object, that is President, at any point of time. Suppose we want to connect to DB from different locations of our code, what we normally do is, we create a DB Connection class and we instatiate it whenever we want to connect to DB. This leads to multiple DB connectios and it becomes costly in terms of load on DB. Instead We create a DB Connection class as Singleton claas. So that only one Instance of DB connection is allowed to instantiate and single connection is established. 
    2. We use Singleton class whenever an application requires only one instance of class, and instance of class should be available to multiple clients. Whenever a client try to access the class, It checks if the instance is already created or not, if not it creates an instance of the Singleton class.
    3. ````
        """
        Singleton Design Pattern
        """
        class Singleton(object):
        def __new__(cls, *args, **kwargs):
        """
        creates an instance of class if not exists
        """
            if not hasattr(cls, '_instance'):
                cls._instance = super().__new__(cls, *args, **kwargs) 
                return cls._instance
            else:
                return "instance is already created!"

        obj1 = Singleton()
        print(obj1)
        # obj2 = Singleton()
        # print(obj2)
        
        ```
    4. Here we have created a Singleton class which uses the dunder method __new__ to create an instance of the class if not exists and stores the instance in class variable '_instance' and returns the else class if we try to create another instance of that class.

1. **Factory Method Pattern**
   1. This Pattern comes under Creational Design Pattern. It is most widely used pattern in application development. In this pattern objects from multiple classes created with in a common interface. When we access the common interface, with the given input the interface decides which subclasses to get instantiated. With the help of Factory Method pattern, we can add new type of functionality without disturbing existing code. Here the subclasses are loosly coupled. Factory methods create objects form concrete classes but return them as objects of abstract type or interface. 
   2. ```
        """
        Factory Method Design Pattern
        Design Pattern Type: Creational Design Pattern
        """

        class BusJourney:
            def __init__(self, name):
                self.name = name

            def confirm_journey(self):
                return f"Hi {self.name} your Bus journey is confirmed!"


        class TrainJourney:
            def __init__(self, name):
                self.name = name

            def confirm_journey(self):
                return f"Hi {self.name} your Train journey is confirmed!"


        class FlightJourney:
            def __init__(self, name):
                self.name = name

            def confirm_journey(self):
                return f"Hi {self.name} your Flight journey is confirmed!"


        # here we used a method as common interface
        def travel_factory(n, o, d, t_type):  # factory method with common data
            set_journey = {"bus": BusJourney,
                        "train": TrainJourney,
                        "flight": FlightJourney,
                        }
            return set_journey[t_type](n)


        if __name__ == "__main__":
            name, origin, dest, travel_type = "Vijay", "Bangalore", "Hyderabad", "train"
            # travel type can be "flight", "train", "bus"
            obj = travel_factory(name, origin, dest, travel_type)
            print(obj.confirm_journey())

            ```
    3. In a travel booking application we have travel options such as flight, train, bus and we don't have to explicitly call that classes everytime we want to create a journey, instead we use the factory method interface to instantiate that classes implicitly and make an abstraction out of it. So in future if want any new type of travel like Sea Journey, we simpley add the new functionality to factory method and the original code will be intact.
1. **Facade Design Pattern**
   1. Facade Design Pattern is a type of Structural Design Pattern. It provides a unified simpler interface for a complex system which has many subsytems. It works as a common layer that isolates the complexity of subsystems from the client. With the Facade Layer in place a unique structure is provided for subsystems by dividing them into layers. With this pattern in place subsystems and client are loosly coupled.
   2. ```
        """
        Pattern: Facade Design Pattern
        Design Pattern Type: Structural Design Pattern
        """

        class Alarm(object):
            """ activating/deactivating alarms first """
            def __init__(self):
                pass

            def activate(self):
                print("Activating Alarm")

            def deactivate(self):
                print("Deactivating Alarm")


        class Smoke(object):
            """ 
            activating/deactivating Smoke effect after Alarm
            """
            def __init__(self):
                pass

            def activate(self):
                print("Activating Smoke")

            def deactivate(self):
                print("Deactivating Smoke")


        class Light(object):
            """ 
            activating/deactivating Lights in emergency exit after smoke effect
            """
            def __init__(self):
                pass

            def activate(self):
                print("Activating Lights")

            def deactivate(self):
                print("Deactivating Lights")


        class FacadePattern():
            """
            encapsulating all classes into one and controlling
            them from single class so that one interface performs
             many other actions in diff classes.
            """
            def __init__(self):
                self._alarm = Alarm()
                self._smoke = Smoke()
                self._lights = Light()

            def activate(self):
                self._alarm.activate()
                self._smoke.activate()
                self._lights.activate()

            def deactivate(self):
                self._alarm.deactivate()
                self._smoke.deactivate()
                self._lights.deactivate()


        if __name__ == '__main__':
            sensor_val = 60  # degree celsius
            obj = FacadePattern()
            if sensor_val > 50:
                obj.activate()  # activating alarm system
            else:
                obj.deactivate()
        ```
    3. In the above code example we have 3 subsystems that are a chain of events to trigger. Without a Facade pattern we need to instantiate each class separately one after the other, with that pattern in place we simply trigger the event by instantiating the Facade class and the class takes care of subsystems one after the other. When Alarm System to be activated in emergency, we call activate method of Facade class and it inturn calls the individual activate methods of subsystems one after other in order of their preference as first Alarm rings, then the Smoke activates then Lights in the Emergency Exit switced on. It is just an example of how a complex subsystems can be operated easily with a Facade layer in between the client and subsytems.

4. **Adapter Design Pattern**
   1. This pattern is a structural design pattern which is used as a bridge between two incompatible objects or classes. This pattern involves a single class which works as a common interface between two independent or incompatible classes.
   2. This has many real world examples. We used to use the adapters for charging our basic phones in old days when we lost the original charger. Where adapter works as a interface between an incompatible charger and a phone. A card reader that we often use that acts as an adapter between memory card and laptop.
   3. ```
        """
        Design Pattern: Adapter Pattern
        Pattern Description: Structural Design Pattern
        """
        class EnglishSpeaker(object):
            def response_to_greeting(self):
                return "Hello, to you too!"

            def response_to_farewell(self):
                return "Goodbye my friend."


        class Translator(object):
            _englishspeaker = None
            _translate_E_to_F = {
                "Hello, to you too!": "Bonjour à vous aussi",
                "Goodbye my friend.": "Au revoir mon ami"
            }

            def __init__(self, englishSpeaker):
                self._englishSpeaker = englishSpeaker


        class FrenchSpeaker(object):
            _engToFreTranslator = None

            def __init__(self, engToFreTranslator):
                self._engToFreTranslator = engToFreTranslator

            def say_hi(self):
                print("Salut!")
                print(self._engToFreTranslator._translate_E_to_F[
                    self._engToFreTranslator._englishSpeaker.response_to_greeting()])

            def say_bye(self):
                print("Au revoir!")
                print(self._engToFreTranslator._translate_E_to_F[
                    self._engToFreTranslator._englishSpeaker.response_to_farewell()
                ])


        englishSpeaker = EnglishSpeaker()
        engToFreTranslator = Translator(englishSpeaker)
        frenchSpeaker = FrenchSpeaker(engToFreTranslator)
        frenchSpeaker.say_hi()
        frenchSpeaker.say_bye()
      ```
    4. In the code example above, when a call a greeting function on French speaker, the Translator has a class variable which is a dictionary of English to French translations of greeting and farewell. When say_hi() method is called for French Speaker, which in turn triggers the translator object which in turn triggers the respond_to_greeting() method of English Speaker object, and the matching translation in the greeting is found in dictionary of translator object which is to be printed to console. In the same way, when say_bye() method is called from French speaker, the English speaker's response is translated with the help of translator object and French version is printed to the console.

5. **Template Method Pattern**
   1. Template Method Design Pattern is one of the behavioural design pattern where skeleton of the application is defined in one abstract class and implementation, modifications, enhancements are to be done by child classes. It is used when group of subclasses want to execute a similar group of methods. There will be an abstract class in which we create a Template method which calls all the individual methods that every subclass/object calls upon. Also the subclasse objects can override the method implementations of template method of abstract class.
   2. The Template Design Patterns are most widely used in Framework development so that, we avoid the code duplication and increase the code reusability. It provides the flexibility to subclasses.
   3. ```
        """
        Design Pattern: Template Method Pattern
        Pattern Description: Behavioral Design Pattern
        """
        class Burger(object):

            def bake_bun(self):
                print("Prepare Dishes\nPreparing Bun.")

            def add_meat(self):
                pass

            def add_vegetables(self):
                pass
            
            def add_condiments(self):
                print("Condiments Added!")

            def wrap_burger(self):
                print("wrapping Burger now.")

            def make_burger(self, ):
                print("Getting Ready!")
                self.bake_bun()
                self.add_meat()
                self.add_vegetables()
                self.add_condiments()
                self.wrap_burger()
                print("Tasty Burger is ready!")
                print()
    

        class VegBurger(Burger):

            def add_vegetables(self):
                print("Vegetables added!")

        class NonVegBurger(Burger):
            def add_meat(self):
                print("Meat Ingredients added.")


        obj1 = VegBurger()
        obj1.make_burger()
        # obj2 = NonVegBurger()
        # obj2.make_burger()

      ```
    1. In the example above, we have a Burger class which is abstract in nature, It has a template method called "make_burger()" which calls all common methods that a subclass object commonly calls upon. When we inherit this Abstract class to our subclasses and we can make any different type of Burgers like for example Veg Burger, Non Veg Burger etc. And we can override the methods in the template method from the subclass also. 







### References:
* [Design Patterns by Sean Bradley](https://medium.com/design-patterns-in-python)
* [geeksforgeeks](https://www.geeksforgeeks.org/python-design-patterns/)
* [Derek Banas]([youtube.com](https://www.youtube.com/playlist?list=PLF206E906175C7E07))
* [Refactoring Guru](https://refactoring.guru/)
* [Tutorials Point](https://www.tutorialspoint.com/python_design_patterns/)